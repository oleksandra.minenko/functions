const getSum = (str1, str2) => {
  if ((typeof str1 !== 'string' || typeof str2 !== 'string') || (isNaN(Number(str1)) || isNaN(Number(str2)))) {
    return false;
  }
  let result = Number(str1) + Number(str2);
  return String(result);
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let post = 0;
  let comments = 0;
  for(let i = 0; i < listOfPosts.length; i++) {
    if(listOfPosts[i]['author'] === authorName) {
      if(listOfPosts[i]['post']) {
        post += 1;
      }
    }
    if(typeof listOfPosts[i]['comments'] === 'object') {
      for(let j = 0; j < listOfPosts[i]['comments'].length; j++) {

        if(listOfPosts[i]['comments'][j]['author'] === authorName) {
          if(listOfPosts[i]['comments'][j]['post']) {
            post += 1;
          }
          if(listOfPosts[i]['comments'][j]['comment']){
            comments += 1;
          }
        }
      }
    }
  }
  return `Post:${post},comments:${comments}`;
};

const tickets=(people)=> {
  for(let i = 0; i < people.length; i++) {
    if ((people[i + 1] - people[i]) <= 25 && people [0] === 25 && people [2] !== 100){
      return 'YES';
    } else {
      return 'NO';
    }
  }
  return 'NO';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
